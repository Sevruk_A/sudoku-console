//
//  sudoku.h
//  QSudoku
//
//  Created by Александр Севрук on 04.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#pragma once

#include <iostream>
#include <cstdlib>

using namespace std;

class Field
{
public:
    Field();
    ~Field();
    
    static const int size = 9;
    int sudoku[size][size];
    int test_array[size*size][size] = {0};
    
    bool checkrow(int, int);
    bool checkcol(int, int);
    bool checksqr(int, int);
    bool test(int, int);
    bool checkRepeated(int i, int j);
    void moveback(int& i, int& j);
    void write(int i, int j);
    void print();
    int hideCells(int hides);
    int complexity();
};