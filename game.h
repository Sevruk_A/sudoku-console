//
//  game.h
//  QSudoku
//
//  Created by Александр Севрук on 04.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#ifndef __QSudoku__game__
#define __QSudoku__game__

#include <iostream>

#include "sudoku.h"

class Game
{
public:
    Game();
    ~Game();
    
    void restart();
    bool checkResultRow();
    bool checkResultColumn();
    void win();
    void game();
};

#endif
