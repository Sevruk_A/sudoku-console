//
//  game.cpp
//  QSudoku
//
//  Created by Александр Севрук on 07.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <string>

#include "game.h"

Game::Game()
{}

Game::~Game()
{}

void Game::game()
{
    Field f;
    
    int menu_2 = f.complexity();
    int hide = 0;
    
    switch (menu_2)
    {
        case 1:
            hide = 5;
            break;
            
        case 2:
            hide = 10;
            break;
            
        case 3:
            hide = 15;
            break;
            
        default:
            cout<<"Такого пункта не существует, попробуйте снова.\n\n";
            game();
            break;
    }
    
    int i, j;
    for(i = 0; i < 9; i++)
    {
        for(j = 0; j < 9; j++)
        {
            for(;;)
            {
                if(!f.test(i, j))
                    f.moveback(i, j);
                
                f.sudoku[i][j] = rand()%9+1;
                
                if(f.checkRepeated(i, j))
                    continue;
                
                f.write(i, j);
                if(f.checksqr(i, j) && f.checkrow(i, j) && f.checkcol(i, j) )
                    break;
            }
        }
    }

    f.hideCells(hide);
    f.print();
    cout << "\n";
    
    int row;
    int col;
    int num;
    
    while (hide > 0)
    {
        cout << "Введите координаты числа и само число: ";
        cin >> row >> col >> num;
        i = row - 1;
        j = col - 1;
        
        if (f.sudoku[i][j] != 0)
        {
            cout << "Эта клетка уже заполнена. Попробуйте снова.\n\n";
        }
        else
        {
            f.sudoku[i][j] = num;
            hide--;
        }
        f.print();
    }
    
    cout << "\n";
    
    if (f.checksqr(i, j) && f.checkrow(i, j) && f.checkcol(i, j))
        win();
    else
    {
        cout << "\nНеправильно заполнена таблица. Вы проиграли. ";
        restart();
    }
       
}

void Game::win()
{
    cout << "Поздравляем, вы прошли игру. ";
    restart();
}

void Game::restart()
{
    cout << "Хотите сыграть еще? Тогда введите 'Да'\n\n";
    string restart;
    cin >> restart;
    
    while (restart != "Да")
    {
        cout << "\nНеправильный ввод. Попробуйте снова.\n";
        cin >> restart;
    }
    
    if (restart == "Да")
    {
        cout << "\n";
        game();
    }
}