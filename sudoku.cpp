//
//  sudoku.cpp
//  QSudoku
//
//  Created by Александр Севрук on 07.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <cstdlib>

using namespace std;

#include "sudoku.h"

Field::Field()
{}

Field::~Field()
{}

int Field::complexity()
{
    
    cout << "Выберите сложность: \n\n";
    cout << "1.Легкая\n";
    cout << "2.Средняя\n";
    cout << "3.Сложная\n\n";
    
    int choice;
    cin >> choice;
    
    cout << "\n";
    
    return choice;
}

bool Field::checkrow(int x, int y)
{
    for(int i = 0; i < y; i++)
        if(sudoku[x][i] == sudoku[x][y])
            return false;
    
    return true;
}

bool Field::checkcol(int x, int y)
{
    for(int i = 0; i < x; i++)
        if(sudoku[i][y] == sudoku[x][y])
            return false;
    
    return true;
}

bool Field::checksqr(int x, int y)
{
    int i_start = x/3;      i_start *= 3;
    int j_start = y/3;      j_start *= 3;
    
    for(int i = i_start; i < i_start + 3; i++)
        for(int j = j_start; j < j_start + 3; j++)
        {
            if(i == x && j==y)
                return true;
            
            if(sudoku[i][j] == sudoku[x][y])
                return false;
        }
    return true;
}

bool Field::test(int i, int j)
{
    int current = i * 9 + j + 1;
    
    for(int x = 1; x < 9; x++)
        if( test_array[current][x] == 0)
            return true;
    
    return false;
}

bool Field::checkRepeated(int i, int j)
{
    int value = sudoku[i][j];
    int current = i * 9 + j + 1;
    
    if(test_array[current][value] == 1)
        return true;
    else
        return false;
}

void Field::moveback(int& i, int& j)
{
    int current = i * 9 + j + 1;
    
    for(int x = 1; x <= 9; x++)
        test_array[current][x] = 0;
    
    if(j < 1)
    {
        i--;    j=8;
    }
    else
        j--;
}

void Field::write(int i, int j)
{
    int current = i * 9 + j + 1;
    int value = sudoku[i][j];
    test_array[current][value] = 1;
}

void Field::print()
{
    int i, j;
    
    for(i = 0; i < 9; i++)
    {
        if( i % 3 == 0 )
            cout << "=========================\n";
        
        cout << "| ";
        
        for(j=0; j<9; j++)
        {
            cout << sudoku[i][j] << " ";
            
            if((j+1)%3 == 0)
                cout << "| ";
        }
        
        cout << endl;
    }
    cout << "=========================\n";
}

int Field::hideCells(int hides)
{
    //int hides = 5;
    while (hides > 0) {
        int i = rand() % 9, j = rand() % 9;
        if (sudoku[i][j] != 0) {
            sudoku[i][j] = 0;
            hides--;
        }
    }
    
    return hides;
}

