//
//  main.cpp
//  QSudoku
//
//  Created by Александр Севрук on 04.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#include "sudoku.h"
#include "game.h"
#include "menu.h"

#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    srand(time(0));
    
    int item = menu();
    std::string answer;
    
    Game g;
    
    switch (item)
    {
        case 1:
            g.game();
            break;

        case 2:
            exit(0);
            break;
            
        default:
            cout<<"Такого пункта не существует, попробуйте снова.\n\n";
            main();
            break;
            
    }
    
    return 0;
}