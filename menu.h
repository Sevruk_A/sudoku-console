//
//  menu.h
//  QSudoku
//
//  Created by Александр Севрук on 04.06.15.
//  Copyright (c) 2015 Александр Севрук. All rights reserved.
//

#ifndef TrackGame_menu_h
#define TrackGame_menu_h

#include <iostream>

int menu()
{
    cout << "Меню игры\n\n";
    cout << "1.Начать игру\n";
    cout << "2.Выход\n\n";
    
    int choice;
    cin >> choice;
    cout << "\n";
    
    return choice;
}

#endif
